(ns picture-gallery.models.db
  (:require [clojure.java.jdbc :as sql]))

(def db (or (System/getenv "DATABASE_URL")
            "postgresql://localhost:5432/gallery"))


(defmacro with-db [f & body]
  `(sql/with-connection ~db (~f ~@body)))

(defn create-user [user]
  (with-db sql/insert-record :users user))

(defn get-user [username]
  (with-db sql/with-query-results
    res ["SELECT * FROM users WHERE username = ?", username]
    (first res)))

(defn add-image [user-id filename caption]
  (with-db
    sql/transaction
    (if (sql/with-query-results
          res
          ["SELECT user_id FROM images WHERE user_id = ? and filename = ?" user-id filename]
          (empty? res))
      (sql/insert-record :images {:user_id user-id :filename filename :caption caption})
      (throw
        (Exception. "You have already uploaded an image with the same filename")))))

(defn images-by-user [user-id]
  (with-db
    sql/with-query-results
    res ["SELECT * FROM images WHERE user_id = ? ORDER BY timestamp DESC" user-id] (doall res)))

(defn get-images [limit offset]
  (with-db
    sql/with-query-results
    res ["SELECT * FROM images ORDER BY timestamp DESC LIMIT ? OFFSET ?" limit offset] (doall res)))

(defn get-user-by-id [id]
  (with-db sql/with-query-results
    res ["SELECT * FROM users WHERE id = ?", id]
    (first res)))

(defn get-image [id]
  (with-db sql/with-query-results
    res ["SELECT * FROM images WHERE id = ?", id]
    (first res)))

(defn delete-image [id]
  (with-db 
    sql/delete-rows :images ["id = ?", id]))

(defn delete-user [id]
  (with-db 
    sql/delete-rows :users ["id = ?", id]))
