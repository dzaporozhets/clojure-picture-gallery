(ns picture-gallery.views.profile
  (:require [hiccup.page :refer [html5 include-css]]
            [hiccup.element :refer :all]
            [clj-time.coerce :as c]
            [clj-time.format :as f]
            [picture-gallery.models.db :as db]
            [noir.session :as session]))

(defn profile-page []
  (let [user (db/get-user (session/get :username))]
    [:div
     [:h1 "Profile"]
     [:div {:class "lead"}
      [:p
       [:span "Username: "]
       [:strong (:username user)]]
      [:p
       [:span "Member since: "]
       [:strong (f/unparse (f/formatters :date) (c/from-date (:timestamp user)))]]]
     [:hr]
     [:form {:action "/profile/delete" :method "POST"} 
      [:button {:class "btn btn-danger btn-sm"} "Delete account"]]]))
