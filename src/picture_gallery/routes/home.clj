(ns picture-gallery.routes.home
  (:require [compojure.core :refer :all]
            [picture-gallery.views.home :as view]
            [picture-gallery.views.layout :as layout]))

(defn home [& page]
  (let [page (read-string (or (first page) "1"))]
    (layout/common (view/gallery page))))

(defroutes home-routes
  (GET "/" [page] (home page)))
