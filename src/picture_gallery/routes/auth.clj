(ns picture-gallery.routes.auth
  (:require [hiccup.form :refer :all]
            [compojure.core :refer :all]
            [picture-gallery.models.db :as db]
            [picture-gallery.routes.home :refer :all]
            [picture-gallery.views.layout :as layout]
            [picture-gallery.util :refer [gallery-path]]
            [noir.validation :as vali]
            [noir.util.crypt :as crypt]
            [noir.session :as session]
            [noir.response :as resp])
  (:import java.io.File))

(defn create-gallery-path []
  (let [user-path (File. (gallery-path))]
    (if-not (.exists user-path) (.mkdirs user-path))
    (str (.getAbsolutePath user-path) File/separator)))

(defn valid? [username password password_confirmation]
  (vali/rule (vali/has-value? username)
             [:username "You should provide username"])
  (vali/rule (vali/min-length? password 6)
             [:password "Password must be at least 6 characters"])
  (vali/rule (= password password_confirmation)
             [:password "Password conformation does not match"])
  (not (vali/errors? :username :password :password_confirmation)))

(defn user-to-session [user]
  (do
    (session/put! :user-id (:id user))
    (session/put! :username (:username user))))

(defn error-item [[error]]
  [:div.text-danger error])

(defn input-control [type id name & [value]]
  [:div.form-group
   (list
     (label id name)
     (vali/on-error (keyword id) error-item)
     (type {:class "form-control"} id value))])

(defn format-error [id ex]
  (cond
    (and (instance? org.postgresql.util.PSQLException ex)
         (= 0 (.getErrorCode ex)))
    (str "User with same username already exists")
    :else
    "An error accured while processing this request"))

(defn login-page [& [username]]
  (layout/common
    [:div.login-form
     [:h1 "Login with existing account"]
     (form-to [:post "/login"]
              (input-control text-field "username" "Username" username)
              (input-control password-field "password" "Password")
              (submit-button {:class "btn btn-success"} "Login"))]))

(defn registration-page [& [username]]
  (layout/common
    [:div.registration-form
     [:h1 "Let's create an account"]
     (form-to [:post "/register"]
              (input-control text-field "username" "Username" username)
              (input-control password-field "password" "Password")
              (input-control password-field "password_confirmation" "Repeat password")
              (submit-button {:class "btn btn-success"} "Create account"))]))

(defn handle-login [username password]
  (let [user (db/get-user username)]
    (if (and user (crypt/compare password (:encrypted_password user)))
      (do
        (user-to-session user)
        (resp/redirect "/"))
      (do
        (vali/rule false [:username "Username or password is invalid"])
        (login-page)))))

(defn handle-logout []
  (session/clear!)
  (resp/redirect "/"))

(defn handle-registration [username password password_confirmation]
  (if (valid? username password password_confirmation)
    (try
      (db/create-user {:username username :encrypted_password (crypt/encrypt password)})
      (user-to-session (db/get-user username))
      (create-gallery-path)
      (resp/redirect "/")
      (catch Exception ex
        (vali/rule false [:username (format-error username ex)])
        (registration-page)))
    (registration-page username)))


(defroutes auth-routes
  (GET "/register" []
       (registration-page))
  (POST "/register" [username password password_confirmation]
        (handle-registration username password password_confirmation))
  (GET "/login" []
       (login-page))
  (POST "/login" [username password]
       (handle-login username password))
  (GET "/logout" []
       (handle-logout)))
