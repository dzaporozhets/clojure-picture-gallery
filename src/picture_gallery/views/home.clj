(ns picture-gallery.views.home
  (:require [hiccup.page :refer [html5 include-css]]
            [hiccup.element :refer :all]
            [picture-gallery.models.db :as db]
            [picture-gallery.util 
             :refer [image-page-uri thumb-prefix image-uri thumb-uri]]
            [noir.session :as session]))

(defn thumbnail-link [{:keys [id user_id filename]}]
  [:div.thumbnail-holder
   [:a {:href (image-page-uri id) :class "thumbnail"}
    (image (thumb-uri user_id filename))]])

(defn images [page]
  (let [offset 20]
    (db/get-images offset (* (- page 1) offset))))

(defn gallery [page]
  [:div
   [:h1 "Hello " (session/get :username)]
   [:p "Here are last images uploaded by users:"]
   [:div.gallery
    (map thumbnail-link (images page))]
   [:div.pager.clearfix
    [:div.page.pull-left "Page: " page]
    [:div.links.pull-right
     (if (pos? (- page 1))
       [:a {:class "btn btn-sm btn-info" :href (str "/?page=" (- page 1))} "Previous page"])
     [:a {:class "btn btn-sm btn-info" :href (str "/?page=" (+ page 1))} "Next page"]]]])
