(ns picture-gallery.views.image
  (:require [hiccup.page :refer [html5 include-css]]
            [hiccup.element :refer :all]
            [clj-time.coerce :as c]
            [picture-gallery.models.db :as db]
            [picture-gallery.util :refer [thumb-prefix image-uri thumb-uri]]
            [noir.session :as session]))

(defn thumbnail-link [{:keys [user_id filename]}]
  [:div.image-holder
   [:a {:href (image-uri user_id filename)}
    (image (image-uri user_id filename))]])

(defn image-data [image author]
  [:div.image-data
   [:h3 (:caption image)]
   [:p 
    [:span "By "]
    [:a {:href (str "/gallery/" (:username author))} (:username author)]]
   [:p
    [:span "Published "]
    [:time.timeago {:datetime (c/from-date (:timestamp image))} (:timestamp image)]]
   (if (= (session/get :user-id) (:user_id image))
     [:div 
      [:hr]
      [:form {:action (str "/images/" (:id image)) :method "POST"} 
       [:button {:class "btn btn-danger btn-sm"} "Delete"]]])])
  

(defn show [image-id]
  (let [image (db/get-image (read-string image-id))]
    [:div.row
     [:div.col-md-8
      (thumbnail-link image)]
     [:div.col-md-4
      (let [author (db/get-user-by-id (:user_id image))]
        (image-data image author))]]))
