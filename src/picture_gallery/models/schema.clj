(ns picture-gallery.models.schema
  (:require [picture-gallery.models.db :refer :all]
            [clojure.java.jdbc :as sql]))


(defn create-users-table []
  (println "Creating database table: users")
  (sql/with-connection db
    (sql/create-table
      :users
      [:id "SERIAL PRIMARY KEY"]
      [:username "varchar(255) NOT NULL UNIQUE"]
      [:encrypted_password "varchar(255)"]
      [:timestamp "TIMESTAMP DEFAULT CURRENT_TIMESTAMP"])))

(defn create-images-table []
  (println "Creating database table: images")
  (sql/with-connection db
    (sql/create-table
      :images
      [:id "SERIAL PRIMARY KEY"]
      [:user_id "INTEGER NOT NULL"]
      [:filename "varchar(255)"]
      [:caption "varchar(255)"]
      [:timestamp "TIMESTAMP DEFAULT CURRENT_TIMESTAMP"])))

(defn table-exists? [name]
  (sql/with-connection db
    (sql/with-query-results
      res
      ["SELECT * FROM information_schema.tables WHERE table_name = ?" name]
      (empty? res))))

(defn migrate []
  (if (table-exists? "users")
    (create-users-table))
  (if (table-exists? "images")
    (create-images-table)))
