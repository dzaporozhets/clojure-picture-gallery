(ns picture-gallery.routes.gallery
  (:require [compojure.core :refer :all]
            [picture-gallery.views.layout :as layout]
            [picture-gallery.views.gallery :as view]))

(defn gallery [username]
  (layout/common (view/gallery-page username)))

(defroutes gallery-routes
  (GET "/gallery/:username" [username] 
       (gallery username)))
