(ns picture-gallery.views.gallery
  (:require [hiccup.page :refer [html5 include-css]]
            [hiccup.element :refer :all]
            [picture-gallery.models.db :as db]
            [picture-gallery.util 
             :refer [image-page-uri thumb-prefix image-uri thumb-uri]]
            [noir.session :as session]))

(defn thumbnail-link [{:keys [id user_id filename]}]
  [:div.thumbnail-holder
   [:div.thumbnail
    [:a {:href (image-page-uri id)}
     (image (thumb-uri user_id filename))]]])

(defn gallery-page [username]
  (let [user (db/get-user username)]
    (or
      [:div.gallery
       [:h2 username " / gallery"]
       (not-empty 
         (map thumbnail-link (db/images-by-user (:id user))))] 
      [:h4 "The user " username " does not have any pictures yet"])))
