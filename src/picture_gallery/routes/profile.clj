(ns picture-gallery.routes.profile
  (:require [compojure.core :refer :all]
            [noir.response :as resp]
            [noir.session :as session]
            [picture-gallery.models.db :as db]
            [picture-gallery.views.layout :as layout]
            [picture-gallery.views.profile :as view]))

(defn remote-user [id]
  (db/delete-user id)
  (session/clear!)
  (resp/redirect "/"))

(defn profile-page []
  (layout/common 
    (view/profile-page)))

(defn delete-profile []
  (remote-user (session/get :user-id)))

(defroutes profile-routes
  (GET "/profile" [] (profile-page))
  (POST "/profile/delete" [] (delete-profile)))
