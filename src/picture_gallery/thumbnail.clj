(ns picture-gallery.thumbnail
  (:require [noir.session :as session]
            [hiccup.util :refer [url-encode]]
            [picture-gallery.util :refer [galleries gallery-path thumb-prefix thumb-uri]]
            [clojure.java.io :as io])
  (:import [java.io File FileInputStream FileOutputStream]
           [java.awt.image AffineTransformOp BufferedImage]
           java.awt.RenderingHints
           java.awt.geom.AffineTransform
           javax.imageio.ImageIO))

(def thumb-size 150)

(defn scale [img ratio width height]
  (let [scale
        (AffineTransform/getScaleInstance
          (double ratio) (double ratio))
        transform-op (AffineTransformOp.
                       scale AffineTransformOp/TYPE_BILINEAR)]
    (.filter transform-op img (BufferedImage. width height (.getType img)))))

(defn scale-image [file]
  (let [img
        (ImageIO/read file)
        img-width (.getWidth img)
        img-height (.getHeight img)
        ratio
        (/ thumb-size img-height)]
    (scale img ratio (int (* img-width ratio)) thumb-size)))

(defn save-thumbnail [{:keys [filename]}]
  (let [path (str (gallery-path) File/separator)]
    (ImageIO/write
      (scale-image (io/input-stream (str path filename)))
      "jpeg"
      (File. (str path thumb-prefix filename)))))
