# Picture gallery application made on Clojure 

[![build status](https://ci.gitlab.com/projects/5702/status.png?ref=master)](https://ci.gitlab.com/projects/5702?ref=master)

![Screenshot_2015-07-16_14.30.00](https://gitlab.com/dzaporozhets/clojure-picture-gallery/uploads/7a3c21da9c63555f0caeb0dd4c96aae9/Screenshot_2015-07-16_14.30.00.png)

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

You will need PostgreSQL installed.

## Install

    git clone https://gitlab.com/clojure-code-examples/picture-gallery.git
    cd picture-gallery

    # Install dependencies
    lein deps

    # Create database
    psql -c 'CREATE DATABASE gallery TEMPLATE template0'

## Running

To start a web server for the application, run:

    lein ring server

## License

The MIT License (MIT)

See [LICENSE](https://gitlab.com/clojure-code-examples/picture-gallery/blob/master/LICENSE) file