(ns picture-gallery.routes.image
  (:require [compojure.core :refer :all]
            [noir.response :as resp]
            [clojure.java.io :as io]
            [noir.session :as session]
            [picture-gallery.util :refer [galleries gallery-path thumb-prefix thumb-uri]]
            [picture-gallery.models.db :as db]
            [picture-gallery.views.image :as view]
            [picture-gallery.views.layout :as layout])
  (:import [java.io File]))


(defn delete-files [image]
  (io/delete-file 
    (str galleries File/separator (:user_id image) File/separator (:filename image)))
  (io/delete-file 
    (str galleries File/separator (:user_id image) File/separator thumb-prefix (:filename image))))

(defn show [id]
  (layout/common (view/show id)))

(defn delete [id]
  (let [image (db/get-image (read-string id))]
    (try
      (db/delete-image (:id image))
      (delete-files image)
      (resp/redirect (str "/gallery/" (session/get :username)))
      (catch Exception ex
        (str "Error uploading file " (.getMessage ex))))))

(defroutes image-routes
  (GET "/images/:id" [id] (show id))
  (POST "/images/:id" [id] (delete id)))
