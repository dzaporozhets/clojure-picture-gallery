(ns picture-gallery.views.upload
  (:require [hiccup.page :refer [html5 include-css]]
            [hiccup.form :refer :all]
            [hiccup.element :refer :all]
            [picture-gallery.models.db :as db]
            [picture-gallery.util :refer [thumb-prefix image-uri thumb-uri]]
            [noir.session :as session]))

(defn upload-page [info]
  [:div {:class "center-form"}
   [:h2 "Upload image"]
   [:p {:class "alert alert-info"} "All uploaded images are public. Anyone can see them"]
   [:p.text-warning info]
   (form-to {:enctype "multipart/form-data" :class "upload-form"}
            [:post "/upload"]
            [:div.form-group
             (list 
               (label :file "Picture")
               (file-upload :file))]
            [:div.form-group
             (list 
               (label :caption "Caption (optional)")
               (text-field {:class "form-control"} :caption))]
            (submit-button {:class "btn btn-success"} "Upload"))])
