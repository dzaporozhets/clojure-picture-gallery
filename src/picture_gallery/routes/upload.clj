(ns picture-gallery.routes.upload
  (:require [compojure.core :refer [defroutes GET POST]]
            [hiccup.element :refer [image]]
            [picture-gallery.models.db :as db]
            [picture-gallery.views.upload :as view]
            [picture-gallery.views.layout :as layout]
            [picture-gallery.util :refer [galleries gallery-path thumb-prefix thumb-uri]]
            [picture-gallery.thumbnail :refer [save-thumbnail]]
            [noir.io :refer [upload-file resource-path]]
            [noir.session :as session]
            [noir.response :as resp]
            [noir.util.route :refer [restricted]]
            [ring.util.codec :refer [url-decode]]
            [ring.util.response :refer [file-response]])
  (:import [java.io File]))

(defn upload-page [info]
  (layout/common (view/upload-page info)))

(defn handle-upload [{:keys [filename] :as file} caption]
  (upload-page
    (if (empty? filename)
      "Please select a file to upload"
      (try 
        (noir.io/upload-file (gallery-path) file :created-path? true) 
        (save-thumbnail file)
        (db/add-image (session/get :user-id) filename caption)
        [:div
         [:p {:class "text-success"} "Successfully uploaded"]
         (image {:height "150px"}
                (thumb-uri (session/get :user-id) filename))
         [:hr]
         [:p "Upload another image?"]]
        (catch Exception ex
          (str "Error uploading file " (.getMessage ex)))))))

(defn serve-file [user-id file-name]
  (file-response (str galleries File/separator user-id File/separator (url-decode file-name))))

(defroutes upload-routes
  (GET "/upload" [info] (restricted (upload-page info)))
  (POST "/upload" [file caption] (restricted (handle-upload file caption)))
  (GET "/img/:user-id/:file-name" [user-id file-name] 
       (serve-file user-id file-name)))
