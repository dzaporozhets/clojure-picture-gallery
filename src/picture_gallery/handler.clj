(ns picture-gallery.handler
  (:require [compojure.core :refer [defroutes routes]]
            [compojure.route :as route]
            [noir.util.middleware :as noir-middleware]
            [picture-gallery.models.schema :as schema]
            [picture-gallery.views.layout :as layout]
            [picture-gallery.routes.gallery :refer [gallery-routes]]
            [picture-gallery.routes.auth :refer [auth-routes]]
            [picture-gallery.routes.image :refer [image-routes]]
            [picture-gallery.routes.upload :refer [upload-routes]]
            [picture-gallery.routes.home :refer [home-routes]]
            [picture-gallery.routes.profile :refer [profile-routes]]
            [noir.session :as session]))

(defn init []
  (do 
    (println "picture-gallery is starting")
    (schema/migrate)))

(defn destroy []
  (println "picture-gallery is shutting down"))

(defn user-page [_]
  (session/get :user-id))

(defn not-found []
  (layout/base
    [:center
    [:img {:width "300" :src "/img/flashpic_black.png"}]
    [:h1 "404. Page not found!"]]))

(defroutes app-routes
  (route/resources "/")
  (route/not-found (not-found)))

(def app 
  (noir-middleware/app-handler 
    [auth-routes 
     home-routes
     image-routes
     gallery-routes
     upload-routes
     profile-routes
     app-routes]
    :access-rules [user-page]))
